package com.time.store;


import com.time.store.auth.entity.bo.Modules;
import com.time.store.auth.entity.bo.RoleModules;
import com.time.store.auth.service.*;
import com.time.store.news.entity.bo.NewsCategory;
import com.time.store.news.entity.bo.NewsInfo;
import com.time.store.news.service.NewsCategoryService;
import com.time.store.news.service.NewsInfoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
public class NewsTests {
    @Autowired
    private NewsCategoryService newsCategoryService;
    @Autowired
    private NewsInfoService newsInfoService;

    @Test
    public void insertNewsInfoAll(){
        List<NewsCategory> list = newsCategoryService.list();
        for (NewsCategory newsCategory : list) {
            for (int i = 0; i < 10; i++) {
                NewsInfo newsInfo=new NewsInfo();
                newsInfo.setTitle("["+newsCategory.getName()+"] 新闻"+(i+1));
                newsInfo.setBody(newsInfo.getBody());
                newsInfo.setLinkUrl("https://www.baidu.com/s?wd="+newsInfo.getTitle());
                newsInfo.setWeight(100-i);
                newsInfo.setNewsCategoryId(newsCategory.getId());
                newsInfoService.save(newsInfo);
            }
        }

    }
}
