package com.time.store.task.entity.bo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 任务
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName("task_store")
@Data
public class TaskStore extends Model<TaskStore> {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 店铺id
     */
    private Integer storeId;

    /**
     * 任务id
     */
    private Integer taskId;

    /**
     * 任务类别id
     */
    private Integer taskCategoryId;

    /**
     * 是否领取奖励 0:未领取 1:已领取
     */
    private Integer isReceive;

    /**
     * 是否已经完成 0:未完成 1:已完成
     */
    private Integer isComplete;


    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
