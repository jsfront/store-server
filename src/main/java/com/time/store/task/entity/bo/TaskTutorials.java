package com.time.store.task.entity.bo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 任务教程
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName("task_tutorials")
@Data
public class TaskTutorials extends Model<TaskTutorials> {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 任务教程标题
     */
    private String title;

    /**
     * 任务教程权重
     */
    private Integer weight;

    /**
     * 任务教程内容json
     */
    private String content;

    /**
     * 任务类别id
     */
    private Integer taskCategoryId;

    /**
     * 任务id
     */
    private Integer taskId;


    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
