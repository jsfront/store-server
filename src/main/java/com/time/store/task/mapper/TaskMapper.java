package com.time.store.task.mapper;

import com.time.store.task.entity.dto.TaskInfoDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Date;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.task.entity.bo.Task;

public interface TaskMapper extends BaseMapper<Task> {

    /**
     * 查询当前店铺权重排序第一个未完成的执行任务
     *
     * @param storeId
     * @return
     */
    TaskInfoDTO selectCurrentNoCompleteByStore(@Param("storeId") Integer storeId);

    /**
     * 查询当前店铺权重排序第一个已经完成的执行任务
     *
     * @param storeId
     * @return
     */
    List<Integer> selectCurrentCompleteByStore(@Param("storeId") Integer storeId);

    /**
     * 查找不属于task的下个权重最高的任务
     *
     * @param taskIds
     */
    TaskInfoDTO selectNoTaskNext(@Param("taskIds") List<Integer> taskIds);
}
