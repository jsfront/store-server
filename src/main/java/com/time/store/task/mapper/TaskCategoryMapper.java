package com.time.store.task.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.task.entity.bo.Task;
import com.time.store.task.entity.bo.TaskCategory;

public interface TaskCategoryMapper extends BaseMapper<TaskCategory> {
}
