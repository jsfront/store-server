package com.time.store.task.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.task.entity.bo.Task;
import com.time.store.task.entity.bo.TaskStore;

public interface TaskStoreMapper extends BaseMapper<TaskStore> {
}
