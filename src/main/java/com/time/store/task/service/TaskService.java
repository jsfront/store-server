package com.time.store.task.service;


import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.auth.entity.base.ActiveUser;
import com.time.store.auth.entity.bo.Permission;
import com.time.store.auth.entity.bo.Users;
import com.time.store.auth.entity.dto.LoginUserReq;
import com.time.store.auth.entity.vo.LoginRes;
import com.time.store.auth.mapper.UsersMapper;
import com.time.store.auth.service.ModulesService;
import com.time.store.auth.service.PermissionService;
import com.time.store.core.config.AuthConfig;
import com.time.store.core.consts.RedisKeyConst;
import com.time.store.core.util.AssertUtil;
import com.time.store.core.util.AuthUtil;
import com.time.store.store.entity.bo.StoreInfo;
import com.time.store.store.service.StoreInfoService;
import com.time.store.task.entity.bo.Task;
import com.time.store.task.entity.dto.TaskInfoDTO;
import com.time.store.task.mapper.TaskMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 * 任务service
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@Service
public class TaskService extends ServiceImpl<TaskMapper, Task> implements IService<Task> {


    public TaskInfoDTO getCurrent() {
        return baseMapper.selectCurrentNoCompleteByStore(AuthUtil.getCurrStoreId());
    }
}
