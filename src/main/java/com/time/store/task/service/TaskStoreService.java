package com.time.store.task.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.task.entity.bo.Task;
import com.time.store.task.entity.bo.TaskStore;
import com.time.store.task.mapper.TaskMapper;
import com.time.store.task.mapper.TaskStoreMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 任务店铺关系service
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@Service
public class TaskStoreService extends ServiceImpl<TaskStoreMapper, TaskStore> implements IService<TaskStore> {


}
