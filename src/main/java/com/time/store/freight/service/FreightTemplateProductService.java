package com.time.store.freight.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.freight.entity.bo.FreightTemplate;
import com.time.store.freight.entity.bo.FreightTemplateProduct;
import com.time.store.freight.mapper.FreightTemplateMapper;
import com.time.store.freight.mapper.FreightTemplateProductMapper;
import org.springframework.stereotype.Service;

@Service
public class FreightTemplateProductService extends ServiceImpl<FreightTemplateProductMapper, FreightTemplateProduct> implements IService<FreightTemplateProduct> {

}
