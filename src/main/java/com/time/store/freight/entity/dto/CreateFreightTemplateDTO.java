package com.time.store.freight.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 创建运费模板
 */
@Data
public class CreateFreightTemplateDTO {
    /**
     * 运费模板名称
     */
    @NotEmpty
    private String name;
    /**
     * 计费方式 (1:按件数 2:按重量)
     */
    @NotNull
    private Integer billingMethod;

    /**
     * 配送区域
     */
    private List<CreateFreightTemplateAreaDTO> templateArea;
}
