package com.time.store.news.entity.dto;


import lombok.Data;

import java.util.Date;

@Data
public class NewsDTO {
    /**
     * id
     */
    private Integer id;

    /**
     * 新闻名称
     */
    private String title;

    /**
     * 分类权重
     */
    private Integer weight;

    /**
     * 内容
     */
    private String body;

    /**
     * 跳转url
     */
    private String linkUrl;
    /**
     * 类别id
     */
    private Integer categoryId;
    /**
     * 类别名称
     */
    private String categoryName;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

}
