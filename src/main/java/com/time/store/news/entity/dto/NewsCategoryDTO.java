package com.time.store.news.entity.dto;


import lombok.Data;

import java.util.List;

@Data
public class NewsCategoryDTO {
    /**
     * 类别id
     */
    private Integer categoryId;
    /**
     * 类别名称
     */
    private String categoryName;

    private List<NewsDTO> news;
}
