package com.time.store.news.controller;

import com.time.store.news.entity.dto.NewsCategoryDTO;
import com.time.store.news.entity.dto.NewsDTO;
import com.time.store.news.service.NewsInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 新闻控制器
 *@description: 新闻
 *@menu 新闻
 */
@RestController
@RequestMapping("/api/news/")
public class NewsController {

    @Autowired
    private  NewsInfoService newsInfoService;

    /**
     * 获取首页右边新闻
     * @status done
     * @return
     */
    @RequestMapping("/getHomeRightNews")
    public List<NewsCategoryDTO> getHomeNews(){
        return newsInfoService.getHomeNews();
    }


    /**
     * 获取功能位置新闻
     * @status done
     * @return
     */
    @RequestMapping("/getFunctionNew")
    public List<NewsDTO> getFunctionNew(){
        return newsInfoService.getFunctionNew();
    }
}
