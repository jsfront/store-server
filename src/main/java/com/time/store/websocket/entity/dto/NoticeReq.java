package com.time.store.websocket.entity.dto;

import lombok.Data;

@Data
public class NoticeReq {
    /**
     * 提示内容
     */
    private String message;
}
