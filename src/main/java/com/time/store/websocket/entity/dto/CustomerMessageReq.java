package com.time.store.websocket.entity.dto;


import lombok.Data;

/**
 * 客户消息
 */
@Data
public class CustomerMessageReq {
    /**
     * 来自哪个客户
     */
    private Integer formUserId;
    /**
     * 消息内容
     */
    private String message;
    /**
     * 消息附件url
     */
    private String fileUrl;
    /**
     * 消息类型(1:文本 2:图片 3:音频 4:视频)
     */
    private Integer messageType;
}
