package com.time.store.websocket.consts;

public class SocketReqCode {
    /**
     * 客户消息
     */
    public static final int CUSTOMER_MESSAGE=1;
    /**
     * 通知
     */
    public static final int NOTICE=2;

}
