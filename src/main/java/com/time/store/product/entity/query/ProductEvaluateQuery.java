package com.time.store.product.entity.query;

import com.time.store.core.entity.base.BasePageRequest;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class ProductEvaluateQuery extends BasePageRequest {
    /**
     * 评价开始时间
     */
    private LocalDateTime createStart;

    /**
     * 评价结束时间
     */
    private LocalDateTime createEnd;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 评价星级 0-5
     */
    @Min(0)
    @Max(5)
    private Integer star;

    /**
     * 评价方式 1:默认评价 2:用户自评
     */
    private Integer evaluateManner;

    /**
     * 是否置顶 1:置顶 2:不置顶
     */
    private Integer isTop;

    /**
     * 评价Tab(1:全部评价 2:用户自评 3:带图评价 4:加精评价 5:默认评价)
     */
    @NotNull
    private Integer evaluateTab;

}
