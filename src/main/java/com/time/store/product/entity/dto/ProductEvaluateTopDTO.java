package com.time.store.product.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 评论置顶
 */
@Data
public class ProductEvaluateTopDTO {
    /**
     * 评价id
     */
    @NotNull
    private Integer id;

}
