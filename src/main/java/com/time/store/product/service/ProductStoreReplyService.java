package com.time.store.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.product.entity.bo.ProductEvaluate;
import com.time.store.product.entity.bo.ProductStoreReply;
import com.time.store.product.mapper.ProductEvaluateMapper;
import com.time.store.product.mapper.ProductStoreReplyMapper;
import org.springframework.stereotype.Service;

@Service
public class ProductStoreReplyService extends ServiceImpl<ProductStoreReplyMapper, ProductStoreReply> implements IService<ProductStoreReply> {
}
