package com.time.store.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.product.entity.bo.ProductCategory;
import com.time.store.product.entity.bo.ProductInfo;
import com.time.store.product.mapper.ProductCategoryMapper;
import com.time.store.product.mapper.ProductInfoMapper;
import org.springframework.stereotype.Service;

@Service
public class ProductCategoryService extends ServiceImpl<ProductCategoryMapper, ProductCategory> implements IService<ProductCategory> {
}
