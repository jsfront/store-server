package com.time.store.product.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.core.util.AuthUtil;
import com.time.store.product.entity.bo.ProductEvaluate;
import com.time.store.product.entity.bo.ProductStoreReply;
import com.time.store.product.entity.dto.ProductEvaluateChoiceDTO;
import com.time.store.product.entity.dto.ProductEvaluateReplyDTO;
import com.time.store.product.entity.dto.ProductEvaluateTopDTO;
import com.time.store.product.entity.query.ProductEvaluateQuery;
import com.time.store.product.entity.vo.ProductEvaluatePageVO;
import com.time.store.product.mapper.ProductEvaluateMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductEvaluateService extends ServiceImpl<ProductEvaluateMapper, ProductEvaluate> implements IService<ProductEvaluate> {
    @Autowired
    private ProductStoreReplyService productStoreReplyService;

    /**
     * 分页查询评价
     *
     * @param productEvaluateQuery
     * @return
     */
    public Page<ProductEvaluatePageVO> pageEvaluate(ProductEvaluateQuery productEvaluateQuery) {
        Page<ProductEvaluatePageVO> page = new Page<>(productEvaluateQuery.getCurrent(), productEvaluateQuery.getSize());
        return baseMapper.pageEvaluate(productEvaluateQuery, page);
    }

    /**
     * 精选
     *
     * @param productEvaluateChoiceDTO
     */
    public void choice(ProductEvaluateChoiceDTO productEvaluateChoiceDTO) {
        lambdaUpdate().set(ProductEvaluate::getIsChoice, 1).eq(ProductEvaluate::getId, productEvaluateChoiceDTO.getId()).update();
    }

    public void top(ProductEvaluateTopDTO productEvaluateTopDTO) {
        lambdaUpdate().set(ProductEvaluate::getIsTop, 1).eq(ProductEvaluate::getId, productEvaluateTopDTO.getId()).update();
    }

    public void reply(ProductEvaluateReplyDTO productEvaluateReplyDTO) {
        ProductStoreReply productStoreReply = new ProductStoreReply();
        productStoreReply.setProductEvaluateId(productEvaluateReplyDTO.getId());
        productStoreReply.setContent(productEvaluateReplyDTO.getContent());
        productStoreReply.setStoreId(AuthUtil.getCurrStoreId());
        productStoreReplyService.save(productStoreReply);
    }
}
