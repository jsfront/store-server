package com.time.store.product.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.time.store.core.util.AuthUtil;
import com.time.store.product.entity.bo.ProductInfo;
import com.time.store.product.entity.dto.ProductInfoAddDTO;
import com.time.store.product.entity.dto.ProductInfoPutOnTheShelfDTO;
import com.time.store.product.entity.dto.ProductInfoUpdateDTO;
import com.time.store.product.entity.query.ProductQuery;
import com.time.store.product.service.ProductInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 商品控制器
 *
 * @description: 商品控制器
 * @menu 商品控制器
 */
@RestController
@RequestMapping("/api/product")
public class ProductController {
    @Autowired
    private ProductInfoService productInfoService;

    /**
     * 分页查询
     * @param productQuery
     * @return
     */
    @GetMapping("/page")
    public Page<ProductInfo> pageProduct(@Validated ProductQuery productQuery) {
        return productInfoService.pageProduct(productQuery);
    }

    /**
     * 新增商品
     *
     * @param productInfoAddDTO
     */
    @PostMapping("")
    public void addProduct(@RequestBody @Validated ProductInfoAddDTO productInfoAddDTO) {
        productInfoService.saveProduct(productInfoAddDTO);
    }

    /**
     * 修改商品
     *
     * @param productInfoUpdateDTO
     */
    @PutMapping("/{id}")
    public void updateProduct(@RequestBody @Validated ProductInfoUpdateDTO productInfoUpdateDTO, @PathVariable("id") Integer id) {
        productInfoUpdateDTO.setId(id);
        productInfoService.updateProduct(productInfoUpdateDTO);
    }


    /**
     * 删除商品
     *
     * @param id 商品id
     */
    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable("id") Integer id) {
        productInfoService.removeById(id);
    }

    /**
     * 上/下架商品
     *
     * @param productInfoPutOnTheShelfDTO
     * @param id                          商品id
     */
    @PatchMapping("/{id}")
    public void putOnTheShelf(@RequestBody @Validated ProductInfoPutOnTheShelfDTO productInfoPutOnTheShelfDTO, @PathVariable("id") Integer id) {
        productInfoPutOnTheShelfDTO.setId(id);
        productInfoService.putOnTheShelf(productInfoPutOnTheShelfDTO);
    }

}
