package com.time.store.product.mapper;
import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.time.store.product.entity.bo.ProductEvaluate;
import com.time.store.product.entity.query.ProductEvaluateQuery;
import com.time.store.product.entity.vo.ProductEvaluatePageVO;
import org.apache.ibatis.annotations.Param;

public interface ProductEvaluateMapper extends BaseMapper<ProductEvaluate> {
    /**
     * 分页查询评价
     *
     * @param productEvaluateQuery
     * @param page
     * @return
     */
    Page<ProductEvaluatePageVO> pageEvaluate(@Param("productEvaluateQuery") ProductEvaluateQuery productEvaluateQuery, @Param("page") Page<ProductEvaluatePageVO> page);



}
