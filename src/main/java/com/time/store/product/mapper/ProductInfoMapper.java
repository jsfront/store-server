package com.time.store.product.mapper;
import java.util.List;
import java.util.Date;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.time.store.product.entity.bo.ProductInfo;
import com.time.store.product.entity.bo.ProductStoreReply;
import com.time.store.product.entity.query.ProductQuery;
import org.apache.ibatis.annotations.Param;

public interface ProductInfoMapper extends BaseMapper<ProductInfo> {


    Page<ProductInfo> pageProduct(@Param("productQuery") ProductQuery productQuery, @Param("page") Page<ProductInfo> page);


}
