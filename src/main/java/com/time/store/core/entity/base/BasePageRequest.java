package com.time.store.core.entity.base;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author 时间
 * @date 2021/3/5 14:16
 */
@Data
public class BasePageRequest extends BaseRequest {
    /**
     * 当前页码
     */
    @NotNull(message = "current 必填 1开始")
    private Integer current;
    /**
     * 总条数
     */
    @NotNull(message = "size 必填")
    private Integer size;
}
