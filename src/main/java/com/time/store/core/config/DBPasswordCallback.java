package com.time.store.core.config;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import com.alibaba.druid.util.DruidPasswordCallback;
import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;
import java.util.Properties;

@Slf4j
public class DBPasswordCallback extends DruidPasswordCallback {
    private final static String pKey ="TZpfoaWnH4Yaq+ErySPgoA==";

    @Override
    public void setProperties(Properties properties) {
        super.setProperties(properties);
        //获取配置文件中的已经加密的密码
        String pwd = (String)properties.get("password");
        if (StrUtil.isNotEmpty(pwd)) {
            try {

                SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, pKey.getBytes(StandardCharsets.UTF_8));
                String passwordDis = aes.decryptStr(pwd, CharsetUtil.CHARSET_UTF_8);
                setPassword(passwordDis.toCharArray());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}