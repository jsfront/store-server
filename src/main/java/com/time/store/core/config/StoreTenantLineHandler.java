package com.time.store.core.config;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import com.baomidou.mybatisplus.extension.plugins.inner.TenantLineInnerInterceptor;
import com.time.store.core.util.AuthUtil;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;

import java.util.List;

public class StoreTenantLineHandler implements TenantLineHandler {
    // 多租户忽略表
    private final List<String> tenantLineIgnoreTables = List.of(
            "system_garbage",
            "store_info",
            "news_position",
            "news_category",
            "news_info",
            "task_info",
            "task_category",
            "task_store",
            "task_tutorials",
            "product_category"
    );

    @Override
    public Expression getTenantId() {
        Integer currUserTenantId = AuthUtil.getRequestStoreId();
        if(ObjectUtil.isNull(currUserTenantId))
            currUserTenantId = AuthUtil.getCurrStoreId();
        // 默认租户0
        if (ObjectUtil.isNull(currUserTenantId))
            return new LongValue(0);
        return new LongValue(currUserTenantId);
    }

    // 这是 default 方法,默认返回 false 表示所有表都需要拼多租户条件
    @Override
    public boolean ignoreTable(String tableName) {
        if (tenantLineIgnoreTables.contains(tableName))
            return true;
        return false;
    }

    @Override
    public String getTenantIdColumn() {
        return "store_id";
    }
}
