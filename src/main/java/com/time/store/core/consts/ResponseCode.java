package com.time.store.core.consts;

public enum ResponseCode {
    success(200, "成功"),
    fail(500, "失败"),
    tokenNotFount(413, "token未找到"),
    tokenInvalidation(416, "token失效"),
    failedToObtainUserInformation(502, "获取用户信息失败"),
    businessException(666, "业务异常"),
    ;
    public int code;
    public String message;

    ResponseCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
