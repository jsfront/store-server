package com.time.store.core.util;

import cn.hutool.core.util.RandomUtil;
import com.time.store.core.consts.DateTimeFormatConst;

import java.util.Date;

public class NoCardUtil {

    public static String generateNo(String prefix) {
        String now = getNowYMdHms();
        String suffix = RandomUtil.randomNumbers(2);
        return prefix + now + suffix;
    }

    public static String getNowYMdHms() {
        return cn.hutool.core.date.DateUtil.format(new Date(), DateTimeFormatConst.yyyyMMddHHmmssSSS);
    }
}
