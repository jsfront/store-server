package com.time.store.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.system.entity.bo.SystemGarbage;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ISystemGarbageMapper extends BaseMapper<SystemGarbage> {
    /**
     * 查询table
     *
     * @return
     */
    List<Map> selectTable(@Param("sql") String sql);
}
