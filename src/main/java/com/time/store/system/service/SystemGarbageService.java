package com.time.store.system.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.core.util.AuthUtil;
import com.time.store.system.entity.bo.SystemGarbage;
import com.time.store.system.mapper.ISystemGarbageMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SystemGarbageService extends ServiceImpl<ISystemGarbageMapper, SystemGarbage> {
    /**
     * 保存删除数据
     *
     * @param table
     * @param deleteSql
     */
    public void saveDeleteData(String table, String deleteSql) {
        String selectSql = deleteSql.replace("delete", "select * ");
        List<Map> rowMaps = baseMapper.selectTable(selectSql);
        rowMaps.forEach(rowMap -> {
            String row = JSON.toJSONString(rowMap);
            SystemGarbage systemGarbage = new SystemGarbage();
            systemGarbage.setTableName(table);
            systemGarbage.setRows(row);
            Integer currUserId = AuthUtil.getCurrUserId();
            if (currUserId == null) {
                currUserId = 0;
            }
            systemGarbage.setOperatingUserId(currUserId);
            systemGarbage.setDeleteSql(deleteSql);
            save(systemGarbage);
        });

    }
}
