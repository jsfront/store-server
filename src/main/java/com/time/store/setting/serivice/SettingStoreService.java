package com.time.store.setting.serivice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.setting.entity.po.SettingProduct;
import com.time.store.setting.entity.po.SettingStore;
import com.time.store.setting.mapper.SettingProductMapper;
import com.time.store.setting.mapper.SettingStoreMapper;
import org.springframework.stereotype.Service;

@Service
public class SettingStoreService extends ServiceImpl<SettingStoreMapper, SettingStore> implements IService<SettingStore> {
}
