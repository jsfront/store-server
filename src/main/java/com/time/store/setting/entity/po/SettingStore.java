package com.time.store.setting.entity.po;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 店铺设置表
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName("setting_store")
@Data
public class SettingStore extends Model<SettingStore> {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 经营状态 (1: 营业 2:休息)
     */
    private int operatingStatus;

    /**
     * 营业时间 前端自定义json
     */
    private String businessTime;

    /**
     * 购物门槛 (1: 不限 2:仅限粉丝购买)
     */
    private int shoppingThreshold;

    /**
     * 购物车 (1: 开启 2:关闭)
     */
    private int shoppingCart;

    /**
     * 联系客服 (1: 开启 2:关闭)
     */
    private int contactCustomerService;

    /**
     * 客服入口商品详情页 (1: 开启 2:关闭)
     */
    private int customerServiceEntranceProductDetail;

    /**
     * 客服入口商品详情页名称
     */
    private String customerServiceEntranceProductDetailName;

    /**
     * 客服入口订单详情页 (1: 开启 2:关闭)
     */
    private int customerServiceEntranceOrderDetail;

    /**
     * 客服入口订单详情页名称
     */
    private String customerServiceEntranceOrderDetailName;

    /**
     * 悬浮滚动栏 (1: 展示 2:不展示)
     */
    private int floatingScrollBar;

    /**
     * 悬浮滚动栏 购买行为 (1: 启用 2: 禁用)
     */
    private int floatingScrollBarPurchaseBehaviour;

    /**
     * 悬浮滚动栏 好评行为 (1: 启用 2: 禁用)
     */
    private int floatingScrollBarFavorablecommentBehaviour;

    /**
     * 办卡/办会员设置 (1: 启用 2: 禁用)
     */
    private int cardSetting;

    /**
     * 店铺黑名单 (1: 开启 2: 关闭)
     */
    private int storeBlack;

    /**
     * 店铺后台水印 (1: 开启 2: 关闭)
     */
    private int storeBackgroundWatermark;

    /**
     * 店铺营销标签 (1: 展示 2: 不展示)
     */
    private int storeMarketingLabel;

    /**
     * 店铺顶部导航 (1: 展示 2: 不展示)
     */
    private int storeTopNavigation;

    /**
     * 店铺底部logo 默认(http://iep-oss.oss-cn-beijing.aliyuncs.com/courpicurl/1545745261906360wallpaper.jpg)
     */
    private String storeBottomLogo;

    /**
     * 微页面默认背景色
     */
    private String defaultBackgroundColorOfMicro;

    /**
     * 页面标题统一后缀 (1:开启 2:不开启)
     */
    private int pageTitleSuffixEnable;

    /**
     * 页面标题统一后缀
     */
    private String pageTitleSuffix;

    /**
     * 店铺名称作为页面标题 (1:作为 2:不作为)
     */
    private int storeNameAsPageTitle;

    /**
     * 售罄商品 (1:展示 2:不作为)
     */
    private int soldOutProduct;

    /**
     * 售罄标识
     */
    private String soldOutLogo;

    /**
     * 店铺id
     */
    private Integer storeId;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
