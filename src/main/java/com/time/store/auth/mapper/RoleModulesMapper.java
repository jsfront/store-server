package com.time.store.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.auth.entity.bo.RoleModules;

/**
 * <p>
 * 角色模块表 Mapper 接口
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
public interface RoleModulesMapper extends BaseMapper<RoleModules> {

}
