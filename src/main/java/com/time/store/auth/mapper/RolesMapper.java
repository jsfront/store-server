package com.time.store.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.time.store.auth.entity.bo.Roles;
import com.time.store.auth.entity.dto.RolePageQueryReq;
import com.time.store.auth.entity.vo.RoleListRes;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
public interface RolesMapper extends BaseMapper<Roles> {


    Page<Roles> pageRole(@Param("rolePage") IPage<Roles> rolePage, @Param("rolePageQueryReq") RolePageQueryReq rolePageQueryReq);

    /**
     * 根据用户id查找角色选中状态
     *
     * @param userId
     * @return
     */
    List<RoleListRes> listByUserId(@Param("userId") Integer userId);
}
