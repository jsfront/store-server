package com.time.store.auth.controller;


import cn.hutool.core.lang.tree.Tree;
import com.time.store.auth.entity.bo.Modules;
import com.time.store.auth.entity.dto.ModulesPageQueryReq;
import com.time.store.auth.entity.dto.UpdateModuleByRoleReq;
import com.time.store.auth.service.ModulesService;
import com.time.store.core.util.AuthUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 菜单控制器
 *
 * @description: 菜单控制器
 * @menu 菜单
 */
@RequestMapping("/api/auth/module")
@RestController
public class ModulesController {
    @Autowired
    private ModulesService modulesService;

    /**
     * 保存菜单
     *
     * @param modules
     * @status done
     */
    @PostMapping("")
    public void save(@RequestBody @Validated Modules modules) {
        modules.setStoreId(AuthUtil.getCurrStoreId());
        modulesService.save(modules);
    }

    /**
     * 修改菜单
     *
     * @param modules
     * @status done
     */
    @PutMapping("/{id}")
    public void update(@RequestBody @Validated Modules modules, @PathVariable("id") Integer id) {
        modules.setId(id);
        modules.setStoreId(null);
        modules.setCreateTime(null);
        modules.setUpdateTime(null);
        modulesService.updateById(modules);
    }

    /**
     * 删除菜单
     *
     * @status done
     */
    @DeleteMapping("/{id}")
    public void remove(@PathVariable("id") Integer id) {
        modulesService.removeById(id);
    }

    /**
     * 查询树形菜单
     *
     * @param modulesPageQueryReq
     * @return
     * @status done
     */
    @GetMapping("/list")
    public List<Tree<Integer>> list(ModulesPageQueryReq modulesPageQueryReq) {
        return modulesService.listModule(modulesPageQueryReq);
    }

    /**
     * 根据角色id查询树形菜单
     *
     * @param roleId
     * @return
     * @status done
     */
    @GetMapping("/listByRole")
    public List<Tree<Integer>> listByRole(@RequestParam Integer roleId) {
        return modulesService.listByRole(roleId);
    }

    /**
     * 修改菜单根据角色
     * @param updateModuleByRoleReq
     */
    @PostMapping("/updateModuleByRole")
    public void updateModuleByRole(@Validated @RequestBody UpdateModuleByRoleReq updateModuleByRoleReq) {
         modulesService.updateModuleByRole(updateModuleByRoleReq);
    }
}
