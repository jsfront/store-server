package com.time.store.auth.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.time.store.auth.entity.bo.Users;
import com.time.store.auth.entity.dto.*;
import com.time.store.auth.service.UsersService;
import com.time.store.core.util.AssertUtil;
import com.time.store.core.util.AuthUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户控制器
 *
 * @description: 用户管理
 * @menu 用户
 */
@RestController
@RequestMapping("/api/auth/user")
@Slf4j
public class UsersController {
    @Autowired
    public UsersService usersService;

    /**
     * 用户查询
     *
     * @param users
     * @return
     * @status done
     */
    @GetMapping("/page")
    public Page<Users> page(UserPageReq users) {
        return usersService.pageUser(users);
    }

    /**
     * 用户增加
     *
     * @param addUserReq
     * @return
     * @status done
     */
    @PostMapping("")
    public void save(@RequestBody @Validated AddUserReq addUserReq) {
        usersService.save(addUserReq);
    }

    /**
     * 用户修改
     *
     * @param updateUserReq
     * @return
     * @status done
     */
    @PutMapping("/{id}")
    public void updateUser(@RequestBody @Validated UpdateUserReq updateUserReq, @PathVariable("id") Integer id) {
        updateUserReq.setId(id);
        usersService.updateUser(updateUserReq);
    }

    /**
     * 用户删除
     *
     * @param id
     * @return
     * @status done
     */
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable("id") Integer id) {
        AssertUtil.isFalse(id.equals(AuthUtil.getCurrUserId()),"不能删除当前用户");
        usersService.removeById(id);
    }

    /**
     * 修改密码
     */
    @PutMapping("/updatePassword")
    public void updatePassword(@RequestBody @Validated UpdatePasswordReq updatePasswordReq) {
        usersService.updatePassword(updatePasswordReq);
    }


    /**
     * 用户个人设置
     *
     * @param settingUserDTO
     */
    @PostMapping("/setting")
    public void setting(@RequestBody @Validated SettingUserDTO settingUserDTO) {
        usersService.setting(settingUserDTO);
    }
}
