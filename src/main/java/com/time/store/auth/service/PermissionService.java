package com.time.store.auth.service;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.auth.entity.bo.Permission;
import com.time.store.auth.entity.bo.RolePermission;
import com.time.store.auth.mapper.PermissionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@Service
public class PermissionService extends ServiceImpl<PermissionMapper, Permission> implements IService<Permission> {

    @Autowired
    private RolesService rolesService;
    @Autowired
    private RolePermissionService rolePermissionService;

    /**
     * @return
     */
    public List<Permission> getPermissionByUserId(Integer userId) {
        List<Integer> roleIds = rolesService.getByUserId(userId);
        if(CollUtil.isEmpty(roleIds)){
            return new ArrayList<>();
        }
        List<Integer> permissionId = rolePermissionService
                .lambdaQuery()
                .in(RolePermission::getRoleId, roleIds)
                .list()
                .stream()
                .map(RolePermission::getPermissionId)
                .collect(Collectors.toList());
        if(CollUtil.isEmpty(permissionId)){
            return new ArrayList<>();
        }
        return lambdaQuery().in(Permission::getId, permissionId).list();
    }
}
