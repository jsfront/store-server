package com.time.store.auth.entity.base;

import cn.hutool.core.lang.tree.Tree;
import com.time.store.auth.entity.bo.Permission;
import com.time.store.auth.entity.bo.Users;
import com.time.store.store.entity.bo.StoreInfo;
import lombok.Data;

import java.util.List;

/**
 * 当前登录用户
 */
@Data
public class ActiveUser {
    /**
     * 当前用户
     */
    private Users activeUsers;
    /**
     * 店铺信息
     */
    private StoreInfo storeInfo;

    /**
     * 用户菜单
     */
    private List<Tree<Integer>> menu;
    /**
     * 用户资源
     */
    private  List<Permission> permissions;
}
