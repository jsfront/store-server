package com.time.store.auth.entity.bo;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 模块表
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName("auth_modules")
@Data
public class Modules extends Model<Modules> {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 模块名称
     */
    @NotNull
    private String name;
    /**
     * 模块code
     */
    @NotNull
    private String code;

    /**
     * 父模块编号(没有父节点 默认 0)
     */
    private Integer parentId;

    /**
     * 模块路径
     */
    private String path;

    /**
     * 权重
     */
    private Integer weight;

    /**
     * 图标
     */
    private String iconClass;
    /**
     * 店铺id
     */
    private Integer storeId;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
