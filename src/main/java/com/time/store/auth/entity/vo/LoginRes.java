package com.time.store.auth.entity.vo;

import lombok.Data;

@Data
public class LoginRes {
    /**
     * token
     */
    private String token;
    /**
     * 刷新Token
     */
    private String refreshToken;
    /**
     * 超时时间(秒)
     */
    private Long expire;
}
