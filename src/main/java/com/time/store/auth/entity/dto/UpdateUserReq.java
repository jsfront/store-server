package com.time.store.auth.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UpdateUserReq {
    private Integer id;

    /**
     * 登录名
     */
    @NotBlank
    private String loginName;
    /**
     * 昵称
     */
    @NotBlank
    private String nickName;

    /**
     * 密码
     */
    @NotBlank
    private String password;

    /**
     * 密保邮箱
     */
    @NotBlank
    private String protectEmail;

    /**
     * 密保手机号
     */
    @NotBlank
    private String protectPhone;
}
