package com.time.store.auth.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class SettingUserDTO {
    /**
     * 昵称
     */
    @NotEmpty
    private String nickName;
    /**
     * 密保邮箱
     */
    @NotEmpty
    private String protectEmail;

    /**
     * 密保手机号
     */
    @NotEmpty
    private String protectPhone;
}
