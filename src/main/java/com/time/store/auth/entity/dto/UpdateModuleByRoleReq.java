package com.time.store.auth.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 给角色设置菜单请求
 */
@Data
public class UpdateModuleByRoleReq {
    /**
     * 角色id
     */
    @NotNull
    private Integer roleId;
    /**
     * 模块id数组
     */
    @NotNull
    private List<Integer> modules;
}
