package com.time.store.order.entity.dto;

import com.time.store.core.entity.base.BasePageRequest;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class AfterSalePageQuery extends BasePageRequest {
    /**
     * 订单code
     */
    private String orderNo;
    /**
     * 申请时间开始
     */
    private LocalDateTime createStart;
    /**
     * 申请时间结束
     */
    private LocalDateTime createEnd;

    /**
     * 售后编号
     */
    private String afterSaleNo;

    /**
     * 退款类型 (1:售中退款 2:售后退款)
     */
    private Integer refundType;

    /**
     * 有赞客服介入状态 (1:未介入 2:介入中 3:介入结束)
     */
    private Integer interveneStatus;

    /**
     * 售后方式 (1:仅退款 2:退货退款 3:换货)
     */
    private Integer manner;

    /**
     * 发货状态 (1:未发货 2:已发货)
     */
    private Integer deliveryStatus;

    /**
     * 售后状态 (1:售后处理中 2:售后申请待商家同意 3:商家不同意售后申请，待买家处理 4:商家同意售后申请，待买家处理 5:买家已退货 6:待商家确认收货 7:商家拒绝收货，待买家处理 8:商家已发货，待买家确认收货，9:售后成功 10:售后关闭)
     */
    private Integer status;

    /**
     * 物流编号
     */
    private String tackingNo;

    /**
     * 物流状态 (1:未签收 2:已签收 3:无物流信息)
     */
    private Integer tackingStatus;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 排序 (1:按最近申请排序,按临近超时排序)
     */
    private Integer sort=1;
}
