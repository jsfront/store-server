package com.time.store.order.entity.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

@Data
public class OrderProductResp {
    /**
     * 订单id
     */
    private Integer orderId;
    /**
     * 订单详情id
     */
    private Integer orderDetailId;
    /**
     * 订单编号
     */
    private String orderNo;
    /**
     * 外部支付编号
     */
    private String externalPayNo;

    /**
     * 订单状态(1:待付款 2:待发货 3:已发货 4:已完成 5:已关闭 6:售后中 7:退款中 8:订单关闭)
     */
    private int status;

    /**
     * 维权状态 (1:未维权 2:维权中 3:维权结束)
     */
    private int rightsProtectionStatus;

    /**
     * 收货人姓名
     */
    private String receiverName;

    /**
     * 收货人手机号
     */
    private String receiverPhone;

    /**
     * 收货人手机号后四位
     */
    private String receiverPhoneLastFour;

    /**
     * 物流记录id
     */
    private Integer shipmentId;

    /**
     * 物流记录编号
     */
    private String shipmentNo;

    /**
     * 物流编号
     */
    private String trackingNo;


    /**
     * 支付方式 (1: 支付宝 2: 微信 3:银联 4:其他)
     */
    private int payMethod;

    /**
     * 订单加星数
     */
    private int payStarred;

    /**
     * 订单类型 (1: 普通订单 2: 代付订单 3:送礼订单 4:送礼社群版订单 5:心愿订单 6:扫码付款 7:酒店订单 8:维权订单 9:周期购订单 10:多人拼团订单 11:知识付费订单)
     */
    private int type;

    /**
     * 订单来源 (1: 浏览器 2:支付宝 3:浏览器 4:商家自有app 5:微信小程序 6: 其他)
     */
    private int source;

    /**
     * 配送方式(1:快递发货 2:上门自提 3:同城配送)
     */
    private int deliveryMethod;

    /**
     * 商品id
     */
    private Integer productId;

    /**
     * 商品编号
     */
    private String productNo;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 商品图片
     */
    private String productPicUrl;

    /**
     * 商品数量
     */
    private Integer qty;

    /**
     * 应收金额
     */
    private BigDecimal receivableAmount;

    /**
     * 优惠金额
     */
    private BigDecimal favorableAmount;

    /**
     * 实收金额
     */
    private BigDecimal paidAmount;

    /**
     * 商品sku信息
     */
    private String productSku;

    /**
     * 下单时间
     */
    private Date createTime;

    public void setReceivableAmount(Integer receivableAmount) {
        this.receivableAmount = BigDecimal.valueOf(receivableAmount).divide(BigDecimal.valueOf(100),2, RoundingMode.HALF_UP);
    }

    public void setFavorableAmount(Integer favorableAmount) {
        this.favorableAmount = BigDecimal.valueOf(favorableAmount).divide(BigDecimal.valueOf(100),2, RoundingMode.HALF_UP);

    }

    public void setPaidAmount(Integer paidAmount) {
        this.paidAmount = BigDecimal.valueOf(paidAmount).divide(BigDecimal.valueOf(100),2, RoundingMode.HALF_UP);

    }
}
