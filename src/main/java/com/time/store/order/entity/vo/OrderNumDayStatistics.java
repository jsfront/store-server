package com.time.store.order.entity.vo;

import lombok.Data;

import java.time.LocalDate;

/**
 * 订单数按日统计
 */
@Data
public class OrderNumDayStatistics {
    /**
     * 下单笔数
     */
    private Integer orderNum;
    /**
     * 支付订单
     */
    private Integer pendingPaymentOrderNum;
    /**
     * 统计日期
     */
    private LocalDate statisticalDate;
}
