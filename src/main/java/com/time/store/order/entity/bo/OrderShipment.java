package com.time.store.order.entity.bo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
@EqualsAndHashCode(callSuper = true)
@TableName("order_shipment")
@Data
public class OrderShipment extends Model<OrderShipment> {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 发货编号
     */
    private String shipmentNo;
    /**
     * 订单编号
     */
    private String orderNo;
    /**
     * 订单ID
     */
    private Integer orderId;


    /**
     * 操作类型(1:单条发货,2: 批量发货)
     */
    private Integer operationType;
    /**
     * 状态(1: 处理中,2: 已完成)
     */
    private Integer  deliveryStatus;

    /**
     * 发货失败原因
     */
    private String failReason;
    /**
     * 物流编号
     */
    private String tackingNo;
    /**
     * 物流名称 中通,申通 等
     */
    private String tackingName;
    /**
     * 操作用户id
     */
    private Integer operatorUserId;
    /**
     * 操作用户登录名
     */
    private String operatorLoginName;
    /**
     * 店铺id
     */
    private Integer storeId;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
