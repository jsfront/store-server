package com.time.store.order.entity.vo;

import lombok.Data;

import java.util.List;

/**
 * 订单概况
 */
@Data
public class OrderOverviewVO {

    /**
     * 待发货订单数
     */
    private Integer toBeDeliveredOrderNum;

    /**
     * 维权订单数
     */
    private Integer rightsProtectionOrderNum;


    /**
     * 待付款订单数
     */
    private Integer pendingPaymentOrderNum;

    /**
     * 七天下单笔数
     */
    private Integer sevenOrderNum;

    /**
     * 七天收入
     */
    private Integer sevenIncome;

    /**
     * 昨日订单数
     */
    private Integer yesterdayOrderNum;
    /**
     * 昨日支付订单数
     */
    private Integer yesterdayPaymentOrderNum;
    /**
     * 七天内下单数和支付数
     */
    private List<OrderNumDayStatistics> sevenOrderNumDayStatistics;
}
