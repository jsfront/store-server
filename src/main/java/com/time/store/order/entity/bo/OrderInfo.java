package com.time.store.order.entity.bo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 订单统计表
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName("order_info")
@Data
public class OrderInfo extends Model<OrderInfo> {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 外部支付编号
     */
    private String externalPayNo;

    /**
     * 订单状态(1:待付款 2:待发货 3:已发货 4:已完成 5:已关闭 6:售后中 7:退款中 8:订单关闭)
     */
    private int status;

    /**
     * 维权状态 (1:未维权 2:维权中 3:维权结束)
     */
    private int rightsProtectionStatus;

    /**
     * 收货人姓名
     */
    private String receiverName;

    /**
     * 收货人手机号
     */
    private String receiverPhone;

    /**
     * 收货人手机号后四位
     */
    private String receiverPhoneLastFour;

    /**
     * 收货人省
     */
    private String receiverProvince;

    /**
     * 收货人市
     */
    private String receiverCity;

    /**
     * 收货人区
     */
    private String receiverArea;
    /**
     * 收货人详细地址
     */
    private String receiverAddress;

    /**
     * 物流记录id
     */
    private Integer shipmentId;

    /**
     * 物流记录编号
     */
    private String shipmentNo;

    /**
     * 物流编号
     */
    private String tackingNo;

    /**
     * 总数量
     */
    private Integer totalQty;

    /**
     * 总应收金额 单位分
     */
    private Integer totalReceivableAmount;

    /**
     * 总优惠金额 单位分
     */
    private Integer totalFavorableAmount;

    /**
     * 总实收金额 单位分
     */
    private Integer totalPaidAmount;

    /**
     * 支付方式 (1: 支付宝 2: 微信 3:银联 4:其他)
     */
    private int payMethod;

    /**
     * 订单加星数
     */
    private int payStarred;

    /**
     * 订单类型 (1: 普通订单 2: 代付订单 3:送礼订单 4:送礼社群版订单 5:心愿订单 6:扫码付款 7:酒店订单 8:维权订单 9:周期购订单 10:多人拼团订单 11:知识付费订单)
     */
    private int type;

    /**
     * 订单来源 (1: 浏览器 2:支付宝 3:浏览器 4:商家自有app 5:微信小程序 6: 其他)
     */
    private int source;

    /**
     * 配送方式(1:快递发货 2:上门自提 3:同城配送)
     */
    private int deliveryMethod;

    /**
     * 下单会员id
     */
    private Integer memberId;

    /**
     * 店铺id
     */
    private Integer storeId;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
