package com.time.store.order.entity.vo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 首页概况
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@Data
public class HomeOverviewVO {

    /**
     * id
     */
    private Integer id;


    /**
     * 待发货订单数
     */
    private Integer forwardingOrderNum;

    /**
     * 维权订单数
     */
    private Integer rightsOrderProtectionNum;

    /**
     * 昨日订单数
     */
    private Integer yesterdayOrderNum;

    /**
     * 昨日交易额
     */
    private Integer yesterdayTransactionAmount;

    /**
     * 可提现余额
     */
    private Integer withdrawalBalance;
    /**
     * 店铺id
     */
    private Integer storeId;

    private Date createTime;
    private Date updateTime;


}
