package com.time.store.order.entity.bo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 订单售后表
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName("order_after_sale")
@Data
public class OrderAfterSale extends Model<OrderAfterSale> {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 售后编号
     */
    private String afterSaleNo;

    /**
     * 商品id
     */
    private Integer productId;

    /**
     * 商品名称
     */
    private Integer productName;

    /**
     * 退货用户id
     */
    private Integer membersId;

    /**
     * 订单id
     */
    private Integer orderId;

    /**
     * 订单code
     */
    private String orderNo;

    /**
     * 订单详情id
     */
    private Integer orderDetailId;

    /**
     * 退款类型 (1:售中退款 2:售后退款)
     */
    private int refundType;

    /**
     * 有赞客服介入状态 (1:未介入 2:介入中 3:介入结束)
     */
    private int interveneStatus;

    /**
     * 售后方式 (1:仅退款 2:退货退款 3:换货)
     */
    private int manner;

    /**
     * 发货状态 (1:未发货 2:已发货)
     */
    private int deliveryStatus;

    /**
     * 售后状态 (1:售后处理中 2:售后申请待商家同意 3:商家不同意售后申请，待买家处理 4:商家同意售后申请，待买家处理 5:买家已退货 6:待商家确认收货 7:商家拒绝收货，待买家处理 8:商家已发货，待买家确认收货，9:售后成功 10:售后关闭)
     */
    private int status;

    /**
     * 物流编号
     */
    private String tackingNo;

    /**
     * 物流状态 (1:未签收 2:已签收 3:无物流信息)
     */
    private int tackingStatus;

    /**
     * 订单金额
     */
    private Integer orderDetailAmount;

    /**
     * 退款金额
     */
    private Integer refundAmount;

    /**
     * 超时时间
     */
    private Date overtimeTime;

    /**
     * 收货人姓名
     */
    private String receiverName;

    /**
     * 收货人手机号
     */
    private String receiverPhone;

    /**
     * 收货人手机号后四位
     */
    private String receiverPhoneLastFour;

    /**
     * 收货人省
     */
    private String receiverProvince;

    /**
     * 收货人市
     */
    private String receiverCity;

    /**
     * 收货人区
     */
    private String receiverArea;
    /**
     * 收货人详细地址
     */
    private String receiverAddress;

    /**
     * 店铺id
     */
    private Integer storeId;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
