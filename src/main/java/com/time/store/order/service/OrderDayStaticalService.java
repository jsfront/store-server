package com.time.store.order.service;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.time.store.order.entity.bo.OrderDayStatistical;
import com.time.store.order.entity.bo.OrderStatistical;
import com.time.store.order.entity.vo.HomeOverviewVO;
import com.time.store.order.entity.vo.OrderNumDayStatistics;
import com.time.store.order.entity.vo.OrderOverviewVO;
import com.time.store.order.mapper.OrderDayStatisticalMapper;
import com.time.store.store.entity.bo.StoreInfo;
import com.time.store.store.service.StoreInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderDayStaticalService extends ServiceImpl<OrderDayStatisticalMapper, OrderDayStatistical> implements IService<OrderDayStatistical> {

    @Autowired
    private OrderStaticalService orderStaticalService;
    @Autowired
    private StoreInfoService storeInfoService;


    public HomeOverviewVO getYesterday() {
        OrderDayStatistical orderDayStatistical = baseMapper.selectByDate(LocalDate.now().minusDays(1));
        OrderStatistical orderStatistical = orderStaticalService.lambdaQuery().last("limit 1").one();
        StoreInfo storeInfo = storeInfoService.lambdaQuery().last("limit 1").one();
        HomeOverviewVO homeOverviewVO = new HomeOverviewVO();
        if (orderStatistical != null) {
            // 店铺待发货订单数
            homeOverviewVO.setForwardingOrderNum(orderStatistical.getToBeDeliveredOrderNum());
            //店铺 维权订单数
            homeOverviewVO.setRightsOrderProtectionNum(orderStatistical.getRightsProtectionOrderNum());
        }

        if (orderDayStatistical != null) {
            // 昨日订单数
            homeOverviewVO.setYesterdayOrderNum(orderDayStatistical.getOrderNum());
            // 昨日交易额
            homeOverviewVO.setYesterdayTransactionAmount(orderDayStatistical.getTransactionAmount());
            homeOverviewVO.setId(orderDayStatistical.getId());
            homeOverviewVO.setCreateTime(orderDayStatistical.getCreateTime());
            homeOverviewVO.setUpdateTime(orderDayStatistical.getUpdateTime());
        }

        // 可提现余额
        homeOverviewVO.setWithdrawalBalance(storeInfo.getWithdrawalMoney());
        homeOverviewVO.setStoreId(storeInfo.getId());

        return homeOverviewVO;
    }

    public OrderOverviewVO orderOverviewVO() {
        // 订单总统计
        OrderStatistical orderStatistical = orderStaticalService.lambdaQuery().last("limit 1").one();
        //订单七天统计
        //昨天
        LocalDate yesterday = LocalDate.now().minusDays(1);
        // 七天
        LocalDate sevenDay = yesterday.minusDays(6);
        List<OrderDayStatistical> orderDayStatisticalList = baseMapper.selectByDateBetween(sevenDay, yesterday);
        OrderOverviewVO orderOverviewVO = new OrderOverviewVO();

        // 订单统计信息
        if (orderStatistical != null) {
            // 待发货订单数
            orderOverviewVO.setToBeDeliveredOrderNum(orderStatistical.getToBeDeliveredOrderNum());
            // 维权订单数
            orderOverviewVO.setRightsProtectionOrderNum(orderStatistical.getRightsProtectionOrderNum());
            // 待付款订单数
            orderOverviewVO.setPendingPaymentOrderNum(orderStatistical.getPendingPaymentOrderNum());
        }

        // 订单日统计信息
        if (CollectionUtil.isNotEmpty(orderDayStatisticalList)) {
            List<OrderNumDayStatistics> orderNumDayStatisticsList = orderDayStatisticalList.stream().map(item -> {
                OrderNumDayStatistics orderNumDayStatistics = new OrderNumDayStatistics();
                orderNumDayStatistics.setStatisticalDate(item.getStatisticalDate());
                orderNumDayStatistics.setOrderNum(item.getOrderNum());
                orderNumDayStatistics.setPendingPaymentOrderNum(item.getPaymentOrderNum());

                // 下单笔数累计
                Integer sevenOrderNum = orderOverviewVO.getSevenOrderNum() == null ? 0 : orderOverviewVO.getSevenOrderNum();
                orderOverviewVO.setSevenOrderNum(sevenOrderNum + orderNumDayStatistics.getOrderNum());

                //收入累计
                Integer sevenIncome = orderOverviewVO.getSevenIncome() == null ? 0 : orderOverviewVO.getSevenIncome();
                orderOverviewVO.setSevenIncome(sevenIncome + item.getIncome());

                //昨日统计
                if (yesterday.equals(orderNumDayStatistics.getStatisticalDate())) {
                    orderOverviewVO.setYesterdayOrderNum(orderNumDayStatistics.getOrderNum());
                    orderOverviewVO.setYesterdayPaymentOrderNum(orderNumDayStatistics.getPendingPaymentOrderNum());
                }
                return orderNumDayStatistics;
            }).collect(Collectors.toList());

            orderOverviewVO.setSevenOrderNumDayStatistics(orderNumDayStatisticsList);
        }

        return orderOverviewVO;
    }
}
