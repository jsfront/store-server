package com.time.store.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.order.entity.bo.OrderDayStatistical;
import com.time.store.order.entity.bo.OrderStatistical;
import com.time.store.order.mapper.OrderDayStatisticalMapper;
import com.time.store.order.mapper.OrderStatisticalMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class OrderStaticalService extends ServiceImpl<OrderStatisticalMapper, OrderStatistical> implements IService<OrderStatistical> {
}
