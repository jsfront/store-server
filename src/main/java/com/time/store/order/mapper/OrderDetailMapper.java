package com.time.store.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.order.entity.bo.OrderDetail;

public interface OrderDetailMapper extends BaseMapper<OrderDetail> {
}
