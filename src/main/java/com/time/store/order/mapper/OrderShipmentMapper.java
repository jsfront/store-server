package com.time.store.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.time.store.order.entity.bo.OrderShipment;
import com.time.store.order.entity.dto.OrderShipmentPageReq;
import org.apache.ibatis.annotations.Param;


public interface OrderShipmentMapper extends BaseMapper<OrderShipment> {
    /**
     * 分页查询发货订单
     *
     * @param orderShipmentPageReq
     * @return
     */
    Page<OrderShipment> pageOrderShipment(@Param("orderShipmentPageReq") OrderShipmentPageReq orderShipmentPageReq,@Param("page") Page<OrderShipmentPageReq> page);
}
