package com.time.store.order.mapper;


import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.time.store.order.entity.bo.OrderDayStatistical;
import com.time.store.order.entity.bo.OrderInfo;
import com.time.store.order.entity.bo.OrderStatistical;
import com.time.store.order.entity.dto.*;
import com.time.store.order.entity.vo.OrderProductResp;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.util.List;

/**
 * 订单mapper
 */
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {
    /**
     * 分页查询订单
     *
     * @param orderPageQuery
     * @return
     */
    Page<OrderProductResp> pageOrder(@Param("orderPageQuery") OrderPageQuery orderPageQuery, Page<OrderProductResp> page);

    /**
     * 订单标准导出
     *
     * @param orderPageQuery
     * @return
     */
    List<OrderStandardExportDTO> exportOrderStandard(@Param("orderPageQuery") OrderPageQuery orderPageQuery);

    /**
     * 订单维度导出
     *
     * @param orderPageQuery
     * @return
     */
    List<OrderExportDTO> exportOrder(@Param("orderPageQuery") OrderPageQuery orderPageQuery);

    /**
     * 订单商品维度导出
     *
     * @param orderPageQuery
     * @return
     */
    List<OrderProductExportDTO> exportOrderProduct(@Param("orderPageQuery") OrderPageQuery orderPageQuery);

    /**
     * 根据订单id查询订单编号
     */
    String getOrderNo(Integer orderId);

    /**
     * 统计订单情况
     *
     * @return
     */
    OrderStatistical orderStatistical();

    /**
     * 根据日期统计订单情况
     * @param
     * @param date
     * @return
     */
    OrderDayStatistical orderStatisticalDay(@Param("date") LocalDate date);

    /**
     * 根据订单编号获取订单信息
     * @param orderNo
     * @return
     */
    OrderInfo selectAllByOrderNo(@Param("orderNo")String orderNo);



}

