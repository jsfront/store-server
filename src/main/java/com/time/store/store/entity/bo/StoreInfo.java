package com.time.store.store.entity.bo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName("store_info")
@Data
public class StoreInfo extends Model<StoreInfo> {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 登录名
     */
    private String name;
    /**
     * 时间币
     */
    private Integer timeMoney;
    /**
     * 余额
     */
    private Integer money;
    /**
     * 可提现余额
     */
    private Integer withdrawalMoney;
    /**
     * 店铺描述
     */
    private String descript;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
