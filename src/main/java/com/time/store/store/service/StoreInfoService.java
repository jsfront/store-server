package com.time.store.store.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.auth.entity.bo.Modules;
import com.time.store.auth.entity.bo.RoleModules;
import com.time.store.auth.service.RoleModulesService;
import com.time.store.auth.service.RolesService;
import com.time.store.store.entity.bo.StoreInfo;
import com.time.store.store.mapper.StoreInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 模块表 服务实现类
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@Service
public class StoreInfoService extends ServiceImpl<StoreInfoMapper, StoreInfo> implements IService<StoreInfo> {

}
